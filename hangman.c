#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct stats{
    int game;
    int win;
    int loss;
    int total_errors;
    double total_time;
    int streak;
    int max_win;
    int max_loss;
};

// only need to cover path to HOME directory
#define MAX_PATH_SIZE 32

// Max wordlength given in asigment is 34 letters long, rounded to even 40
#define MAX_WORD_SIZE 40

void file_open(FILE *fp, char *word);
void user_guess(char *guess);
void get_stats(char *file_buf, struct stats *user);
void print_hangman(int errors);
void print_stats(struct stats *user);

int main(int argc, char *argv[])
{
    struct stats user;
    user.game = 1;
    user.win = 0;
    user.loss = 0;
    user.total_errors = 0;
    user.total_time = 0;
    user.streak = 0;  // postivie is win streak negitive is loss streak
    user.max_win = 0;
    user.max_loss = 0;
    

    FILE *stats;

    // Max wordlength given in asigment is 34 letters long, rounded to even 40
    char secret[MAX_WORD_SIZE];
    char stat_path[MAX_PATH_SIZE];

    // SEED the RN Jesus
    srand(time(NULL));

    time_t start = time(NULL);

    const char *name = "HOME";
    char *tmp_path = getenv(name);
    strncpy(stat_path, tmp_path, sizeof(stat_path));
    strncat(stat_path, "/.hangman",sizeof(stat_path) - strlen(stat_path) - 1);
    // If .hangman exists use those stats for the play through other wise default all to 0
    // Checking for file existance using access taken from :
    // stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c-cross-platform
    if (access(stat_path, F_OK) != -1){
        char file_buff[MAX_WORD_SIZE];
        stats = fopen(stat_path,"r");
        if(!stats){
            printf("Unable to open stat file  for reading.\n");
        }
        fgets(file_buff,sizeof(file_buff),stats);
        //printf("%s",file_buff);
        if(fclose(stats)){
            printf("Unable to close Statistics file!?");
        }
        get_stats(file_buff,&user);
    }

    print_stats(&user);

    // Opens file and picks secret number
    switch(argc){
        case 1:;    //DO nothing for case stament to work with variable declaraton
            // Read from ~/.word file
            // Getting environemtal variable for HOME
            // obtained from pubs.opengroup.org/onlinepubs/9699919799/functions.getenv.html
            char home_path[MAX_PATH_SIZE];
            strncpy(home_path, tmp_path, sizeof(home_path));
            strncat(home_path, "/.words",sizeof(home_path)- strlen(home_path)- 1);
            FILE *fp = fopen(home_path,"r");
            file_open(fp, secret);
            if(!fp){
                printf("Unable to open ~/.words for reading.\n");
                return 1;
            }
            break;
        case 2:;    //DO nothing for case stament to work with variable declaraton
            fp = fopen(argv[1],"r");
            file_open(fp, secret);
            if(!fp){
                printf("Unable to open user supplied file for reading.\n");
                return 1;
            }
            break;
        default:
            printf("Too many arguments");
            return 1;
    }
    // Removing the \n from the word read from the file
    secret[strlen(secret)-1] = '\0';
    int length = strlen(secret);

    // Setting the letter_right to display only "_" when you start
    char *letters_right = malloc((length+1)*sizeof(char));

    for(int i = 0; i <= length; ++i){
        if(i == length){
            letters_right[i] = '\0';
        }else{
            letters_right[i] = '_';
        }
    }

    char guess;
    bool guess_correct;
    int errors = 0;

    // You may now play the game
    while(errors < 6 && strcmp(secret,letters_right) != 0){
        if(errors != 0){
            print_hangman(errors);
        }
        guess_correct = false;
        // Printing the errors and the array showing correct letters
        printf("%d %s:",errors,letters_right);
        user_guess(&guess);

        // Checking the user's character against the secret word
        for(int i = 0; i < length; ++i){
            //printf("%c-%c\n",secret[i],guess);
            if (secret[i] == guess){
                letters_right[i] = guess;
                guess_correct = true;
            }
        }
        if(!guess_correct){
            ++errors;
        }
    }
    // End game did you win or losse. Probably you lost.
    if(strcmp(secret,letters_right) == 0){
        // What is this a participation medal, only counting errors if they win
        // Losses should still add to errors, but the LIAM's insructions said no
        user.total_errors += errors;
        printf("Secret Word: %s\n",secret);
        printf("You Beat The Game!!");
        if (errors <= 1){
            printf("  You had %d miss.\n",errors);
        }else{
            printf("  You had %d misses.\n",errors);
        }
        user.win += 1;
        if(user.streak >= 0){
            user.streak += 1;
        }else{
            user.streak = 1;
        }
        if(user.streak > user.max_win){
            user.max_win = user.streak;
        }
    }else{
        printf(" o\n/|\\\n/ \\\n");
        printf("Secret Word: %s\n",secret);
        printf("YOU LOST.\n");
        user.loss += 1;
        if (user.streak <= 0){
            user.streak -= 1;
        }else{
            user.streak = -1;
        }
        if(abs(user.streak) > user.max_loss){
            user.max_loss = abs(user.streak);
        }
    }
    // Free the malloc or valgrind won't be happy
    free(letters_right);
    // What is you end time
    time_t stop = time(NULL);
    // Calculate time played and totals accross games
    double time_played = difftime(stop,start);
    user.total_time += time_played;

    // Writing the new stats into the file, "w" clears the file when it opens it
    stats = fopen(stat_path,"w");
    fprintf(stats,"%d %d",user.game,user.win);
    fprintf(stats," %d %d", user.loss,user.total_errors);
    fprintf(stats," %d %lf", user.streak, user.total_time);
    fprintf(stats," %d %d", user.max_win, user.max_loss);
    if(fclose(stats)){
        printf("Unable to close Statistics file!?");
    }
}

void file_open(FILE *fp, char *word)
{
    // Coutning the number of lines in a file and randomly selecting one word to use
    // code adopted from stackoverflow.com/questions/232237/whats-the-best-way-to-return-a-random-line-in-a-text-file-using-c
    int num_lines = 0;
    while(!feof(fp)){
        fgets(word,sizeof(word), fp);
        ++num_lines;
    }
    while(1){
        int rand_line = rand() % num_lines;

        fseek(fp, 0, SEEK_SET);
        for(int i = 0; !feof(fp) && i <= rand_line; ++i){
            fgets(word, sizeof(word), fp);
        }
        size_t i = 0;
        while(isalpha(word[i])){
            ++i;
        }
        // Discarding and randomly selecting new word if selction is not
        // a single word(if line selected contain and non-alphabetic characters
        if (i == strlen(word)-1){
            // MUST CLOSE FILE the valgrind overloards command it
            if(fclose(fp)){
                printf("Unable to close word list file!?");
            }
            break;
        }
    }
}

void user_guess(char *guess)
{
    while(1){
        char buff[8]; // Allow them to misskey a few characters
        fgets(buff,sizeof(buff),stdin);
        // I'll be nice if they fat finger they can have a redo on their guess
        if (strlen(buff) == 2){
            *guess = buff[0];
            *guess = tolower(*guess);
            break;
        }
        printf("Entered more than one Character, enter again:");
    }
    // No redo if they decide to key in a non-alphabetic charater however
    // I'm not THAT nice
    if(!(isalpha(*guess))){
        printf("%c is not a Alphabetic Character\n",*guess);
    }
}

void get_stats(char *file_buff, struct stats *user)
{
    // Breaking the data string of user stats from the file into each part of the struct
    char *tmp = strtok(file_buff, " ");
    user->game = strtol(tmp,NULL,10);
    int stat_num = 2;
    while(tmp != NULL){
        tmp = strtok(NULL, " ");
        //printf("%s",tmp);
        switch(stat_num){
            case 2:
                user->win = strtol(tmp,NULL,10);
            case 3:
                user->loss = strtol(tmp,NULL,10);
            case 4:
                user->total_errors = strtol(tmp,NULL,10);
            case 5:
                // postivie is win streak negitive is loss streak
                user->streak = strtol(tmp,NULL,10);
            case 6:
                user->total_time = strtol(tmp,NULL,10);
            case 7:
                user->max_win = strtol(tmp,NULL,10);
            case 8:
                user->max_loss = strtol(tmp,NULL,10);
            default:
                break;
        }
        ++stat_num;
    }
}

void print_hangman(int errors)
{
    char hangman[6]= {'o','/','|','\\','/','\\'};
    //print the hanging man
    printf(" ");
    for(int i = 0; i < errors; ++i){
        if(i == 0 || i == 3){
            printf("%c\n",hangman[i]);
        }else if(errors == 2 && i == 1){
            printf(" %c",hangman[2]);
        }else if(i == 5){
            printf(" %c\n",hangman[i]);
        }else{
            printf("%c",hangman[i]);
        }
    }
    printf("\n");
}

void print_stats(struct stats *user)
{
    double avg_score = 0;
    double avg_time = 0;
    avg_score = (double)user->total_errors/(double)user->game;
    avg_time = user->total_time/(double)user->game;

    //Printing the statistics from the .hangman file default is to be all zeros
    printf("Game %d\n", user->game);
    if(user->win == 1 && user->loss != 1){
        printf("%d Win/%d Losses", user->win,user->loss);
    }else if(user->win != 1 && user->loss == 1){
        printf("%d Wins/%d Loss", user->win,user->loss);
    }else if(user->win == 1 && user->loss == 1){
        printf("%d Win/%d Loss", user->win,user->loss);
    }else{
        printf("%d Wins/%d Losses ", user->win,user->loss);
    }
    printf("\nAverage Score: %.2lf\n", avg_score);
    printf("Total time played (s): %.1lf\n", user->total_time);
    printf("Average Game Length (s): %.1lf\n", avg_time);
    if (user->streak > 0){
        printf("Streak: %d W\n", user->streak);
    }else if(user->streak < 0){
        printf("Streak: %d L\n", abs(user->streak));
    }else{
        printf("Streak: none\n");
    }
    printf("Longest Win  Streak: %d\n", user->max_win);
    printf("Longest Loss Streak: %d\n", user->max_loss);
}
