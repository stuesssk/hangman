CPPFLAGS+=-Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

CFLAGS+=-std=c11
CFLAGS+=-g

BINS=hangman hangman.o

.PHONY: all clean

all: $(BINS)

clean:
	$(RM) $(BINS)

